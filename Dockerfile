FROM alpine:3.11.3

RUN apk add --update \
    nginx \
    tzdata \
    ca-certificates \
    && rm -rf /var/cache/apk/*

WORKDIR /html

RUN ln -s /dev/null /var/log/nginx/access.log && \
    ln -s /dev/stdout /var/log/nginx/error.log && \
    cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime && \
    echo "Europe/Berlin" > /etc/timezone && \
    (crontab -l ; echo "* * * * * php /html/artisan schedule:run >> /dev/null 2>&1") | crontab -

WORKDIR /html
EXPOSE 80

COPY . /html
COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/nginx-default.conf /etc/nginx/conf.d/default.conf

CMD nginx